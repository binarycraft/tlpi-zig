//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 19-1                                                            !
//!                                                                         !
//! demo_inotify.zig                                                        !
//!                                                                         !
//! Demonstrate the use of the inotify API.                                 !
//!                                                                         !
//! Usage: demo_inotify pathname...                                         !
//!                                                                         !
//! The program monitors each of the files specified on the command line    !
//! for allpossible file events.                                            !
//!                                                                         !
//! This program is Linux-specific. The inotify API is available in Linux   !
//! 2.6.13 and later.                                                       !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const print = std.debug.print;

const BUF_LEN = 10 * @sizeOf(os.system.inotify_event) + os.NAME_MAX + 1;

// Display information from inotify_event structure
pub fn displayInotifyEvent(i: *os.system.inotify_event) void {
    print("    wd ={d}; ", .{i.wd});
    if (i.cookie > 0) {
        print("cookie ={d}; ", .{i.cookie});
    }

    print("mask = ", .{});
    if ((i.mask & os.system.IN.ACCESS) > 0) print("IN_ACCESS ", .{});
    if ((i.mask & os.system.IN.ATTRIB) > 0) print("IN_ATTRIB ", .{});
    if ((i.mask & os.system.IN.CLOSE_NOWRITE) > 0) print("IN_CLOSE_NOWRITE ", .{});
    if ((i.mask & os.system.IN.CLOSE_WRITE) > 0) print("IN_CLOSE_WRITE ", .{});
    if ((i.mask & os.system.IN.CREATE) > 0) print("IN_CREATE ", .{});
    if ((i.mask & os.system.IN.DELETE) > 0) print("IN_DELETE ", .{});
    if ((i.mask & os.system.IN.DELETE_SELF) > 0) print("IN_DELETE_SELF ", .{});
    if ((i.mask & os.system.IN.IGNORED) > 0) print("IN_IGNORED ", .{});
    if ((i.mask & os.system.IN.ISDIR) > 0) print("IN_ISDIR ", .{});
    if ((i.mask & os.system.IN.MODIFY) > 0) print("IN_MODIFY ", .{});
    if ((i.mask & os.system.IN.MOVE_SELF) > 0) print("IN_MOVE_SELF ", .{});
    if ((i.mask & os.system.IN.MOVED_FROM) > 0) print("IN_MOVED_FROM ", .{});
    if ((i.mask & os.system.IN.MOVED_TO) > 0) print("IN_MOVED_TO ", .{});
    if ((i.mask & os.system.IN.OPEN) > 0) print("IN_OPEN ", .{});
    if ((i.mask & os.system.IN.Q_OVERFLOW) > 0) print("IN_Q_OVERFLOW ", .{});
    if ((i.mask & os.system.IN.UNMOUNT) > 0) print("IN_UNMOUNT ", .{});
    print("\n", .{});

    // name member is missing in zig implementation
    //if (i.len > 0) {
    //    print(" name = %s\n", .{i.name});
    //}
}

pub fn main() !void {
    const argv = os.argv;
    var buf: [BUF_LEN]u8 = undefined;

    if (argv.len < 2 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} pathname...\n", .{argv[0]});
        return;
    }

    var inotifyFd = try os.inotify_init1(0); // Create inotify instance

    // For each command-line argument, add a watch for all events
    for (argv[1..]) |arg| {
        var wd = try os.inotify_add_watch(inotifyFd, mem.span(arg), os.system.IN.ALL_EVENTS);
        print("Watching {s} using wd {d}\n", .{ arg, wd });
    }

    while (true) { // Read events forever
        const numRead = try os.read(inotifyFd, &buf);
        print("Read {d} bytes from inotify fd\n", .{numRead});

        // Process all of the events in buffer returned by read()
        var slice: []u8 = buf[0..numRead];
        const alignment = @alignOf(*os.system.inotify_event);
        while (@ptrToInt(slice.ptr) < @ptrToInt(&buf[numRead])) {
            const event = @ptrCast(*os.system.inotify_event, @alignCast(alignment, slice.ptr));
            defer slice.ptr += @sizeOf(os.system.inotify_event) + event.len;
            displayInotifyEvent(event);
        }
    }
}
