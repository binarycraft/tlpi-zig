const std = @import("std");
const Thread = std.Thread;
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const print = std.debug.print;

var glob: usize = 0;
var rwlock = Thread.RwLock{};

pub fn main() !void {
    const argv = os.argv;
    var loops = try fmt.parseInt(usize, mem.span(argv[1]), 0);

    const thread1 = try Thread.spawn(.{}, threadFunc, .{&loops});
    const thread2 = try Thread.spawn(.{}, threadFunc, .{&loops});

    thread1.join();
    thread2.join();

    print("glob = {d}\n", .{glob});
}

fn threadFunc(arg: *const usize) void {
    var loops = arg.*;
    var j: usize = 0;
    while (j < loops) : (j += 1) {
        rwlock.lock();
        defer rwlock.unlock();

        var loc = glob;
        loc += 1;
        glob = loc;
    }
}
