const std = @import("std");
const os = std.os;
const fmt = std.fmt;
const mem = std.mem;
const time = std.time;
const daemons = @import("daemons.zig");

pub fn main() !void {
    const argv = os.argv;
    try daemons.becomeDaemon(0);

    if (argv.len > 1) {
        const sleepTime = try fmt.parseInt(
            usize,
            mem.span(argv[1]),
            0,
        );
        time.sleep(sleepTime * 1000000000);
    } else {
        time.sleep(20 * 1000000000);
    }
}
