//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//!  Solution for Exercise 5-3                                              !
//!                                                                         !
//!  atomic_append.zig                                                      !
//!                                                                         !
//!  Demonstrate the difference between using nonatomic lseek()+write()     !
//!  and O_APPEND when writing to a file.                                   !
//!                                                                         !
//!  Usage: file num-bytes [x]                                              !
//!                                                                         !
//!  The program write 'num-bytes' bytes to 'file' a byte at a time. If     !
//!  no additional command-line argument is supplied, the program opens the !
//!  file with the O_APPEND flag. If a command-line argument is supplied,   !
//!  the! O_APPEND is omitted when calling open(), and the program calls    !
//!  lseek() to seek to the end of the file before calling write(). This    !
//!  latter technique is vulnerable to a race condition, where data is lost !
//!  because the lseek() + write() steps are not atomic. This can be        !
//!  demonstrated by looking at the size of the files produced by these two !
//!  commands:                                                              !
//!                                                                         !
//!       atomic_append f1 1000000 & atomic_append f1 1000000               !
//!                                                                         !
//!       atomic_append f2 1000000 x & atomic_append f2 1000000 x           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const print = std.debug.print;

pub fn main() !void {
    const argv = os.argv;
    if (argv.len < 3 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} file num-bytes [x]\n        'x' means use lseek() instead of O_APPEND\n", .{argv[0]});
        return;
    }
    const useLseek = argv.len > 3;
    var flags: u32 = if (useLseek) 0 else os.O.APPEND;
    var numBytes = try fmt.parseInt(usize, mem.span(argv[2]), 0);

    const fd = try os.open(mem.span(argv[1]), os.O.RDWR | os.O.CREAT | flags, os.S.IRUSR | os.S.IWUSR);
    defer os.close(fd); // close file after program exits

    var j: usize = 0;
    while (j < numBytes) : (j += 1) {
        if (useLseek) {
            try os.lseek_END(fd, 0);
        }
        _ = try os.write(fd, "x");
    }
    try os.fsync(fd);
    print("{d} done\n", .{os.system.getpid()});
}
