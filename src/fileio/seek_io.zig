//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!
//!  Listing 4-3
//!
//!  seek_io.zig
//!
//!  Demonstrate the use of lseek() and file I/O system calls.
//!
//!  Usage: seek_io file {r<length>|R<length>|w<string>|s<offset>}...
//!
//!  This program opens the file named on its command line, and then performs
//!  the file I/O operations specified by its remaining command-line arguments:
//!
//!          r<length>    Read 'length' bytes from the file at current
//!                       file offset, displaying them as text.
//!
//!          R<length>    Read 'length' bytes from the file at current
//!                       file offset, displaying them in hex.
//!
//!          w<string>    Write 'string' at current file offset.
//!
//!          s<offset>    Set the file offset to 'offset'.
//!
//!  Example:
//!
//!       seek_io myfile wxyz s1 r2
//!

const std = @import("std");
const os = std.os;
const fmt = std.fmt;

pub fn main() !void {
    const argv = os.argv;

    if (argv.len < 3 or std.mem.eql(u8, std.mem.span(argv[1]), "--help")) {
        std.debug.print("{s} file {{r<length>|R<length>|w<string>|s<offset>...}}\n", .{argv[0]});
    }

    const fd = try os.open(std.mem.span(argv[1]), os.O.RDWR |
        os.O.CREAT, os.S.IRUSR | os.S.IWUSR | os.S.IRGRP |
        os.S.IWGRP | os.S.IROTH | os.S.IWOTH); // rw-rw-rw-
    defer os.close(fd);

    var ap: u8 = 2;
    const allocator = std.heap.page_allocator;
    while (ap < argv.len) : (ap += 1) {
        var argv_ap = std.mem.span(argv[ap]);
        switch (argv_ap[0]) {
            'r', 'R' => { // Display bytes at current offset, as text or hex
                var len = try fmt.parseInt(usize, argv_ap[1..argv_ap.len], 0);
                var buf = try allocator.alloc(u8, len);
                defer allocator.free(buf);
                var numRead = try os.read(fd, buf);
                if (numRead == 0) {
                    std.debug.print("{s}: end-of-file\n", .{argv_ap});
                } else {
                    var j: usize = 0;
                    std.debug.print("{s}: ", .{argv_ap});
                    while (j < numRead) : (j += 1) {
                        if (argv[ap][0] == 'r') {
                            std.debug.print("{c}", .{if (std.ascii.isPrint(buf[j])) buf[j] else '?'});
                        } else {
                            std.debug.print("{x} ", .{buf[j]});
                        }
                    }
                    std.debug.print("\n", .{});
                }
            },
            'w' => {
                var numWritten = try os.write(fd, argv_ap[1..argv_ap.len]);
                try os.fsync(fd);
                std.debug.print("{s}: wrote {d} bytes\n", .{ argv_ap, numWritten });
            },
            's' => {
                var offset = try fmt.parseInt(u64, argv_ap[1..argv_ap.len], 0);
                try os.lseek_SET(fd, offset);
                std.debug.print("{s}: seek succeeded\n", .{argv_ap});
            },
            else => {
                std.debug.print("Argument must start with [rRws]: {s}\n", .{argv_ap});
            },
        }
    }
}
