//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 57-6                                                            !
//!                                                                         !
//! ud_ucase_sv.zig                                                         !
//!                                                                         !
//! A server that uses a UNIX domain datagram socket to receive             !
//! datagrams, convert their contents to uppercase, and then                !
//! return them to the senders.                                             !
//!                                                                         !
//! See also ud_ucase_cl.zig.                                               !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;
const system = os.system;

const SV_SOCK_PATH = "/tmp/ud_ucase";
const BUF_SIZE = 10;

pub fn main() !void {
    var buf: [BUF_SIZE]u8 = undefined;
    var claddr: net.Address = undefined;
    // Create server socket
    const sockfd = try os.socket(
        os.AF.UNIX,
        os.SOCK.DGRAM,
        0,
    );

    // Construct server socket address, bind socket to it
    os.unlink(SV_SOCK_PATH) catch |err| switch (err) {
        error.FileNotFound => {},
        else => {
            return err;
        },
    };

    var svaddr = try std.net.Address.initUnix(SV_SOCK_PATH);

    try os.bind(sockfd, &svaddr.any, svaddr.getOsSockLen());

    // Receive messages, convert to uppercase, and return to client
    while (true) {
        var rev_len = @intCast(os.socklen_t, @sizeOf(os.sockaddr.un));
        var bytesReceived = try os.recvfrom(sockfd, &buf, 0, &claddr.any, &rev_len);
        print("Server received {d} bytes from {s}\n", .{ bytesReceived, claddr.un.path });

        for (buf[0..bytesReceived]) |*char| {
            char.* = ascii.toUpper(char.*);
        }

        var bytesSent = try os.sendto(sockfd, buf[0..bytesReceived], 0, &claddr.any, rev_len);
        if (bytesSent != bytesReceived) {
            log.warn("Failed to send all data", .{});
        }
    }
}
