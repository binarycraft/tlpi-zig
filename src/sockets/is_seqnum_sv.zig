//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 59-6                                                            !
//!                                                                         !
//! is_seqnum_sv.zig                                                        !
//!                                                                         !
//! A simple Internet stream socket server. Our service is to provide       !
//! unique sequence numbers to clients.                                     !
//!                                                                         !
//! Usage:  is_seqnum_sv [init-seq-num]                                     !
//!                      (default = 0)                                      !
//!                                                                         !
//! See also is_seqnum_cl.zig.                                              !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const builtin = @import("builtin");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;

const BUF_SIZE = 256;
const PORT_NUM = 50000;

pub const log_level: std.log.Level = .info;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();

    const argv = try std.process.argsAlloc(gpa.allocator());
    defer std.process.argsFree(gpa.allocator(), argv);

    if (argv.len > 1 and mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} [init-seq-num]\n", .{argv[0]});
        return;
    }

    var seqNum = if (argv.len > 1) try fmt.parseInt(
        usize,
        mem.span(argv[1]),
        0,
    ) else 0;

    // Not implemented for windows yet
    if (builtin.os.tag != .windows) {
        const sigact = os.Sigaction{
            .handler = .{ .handler = os.SIG.IGN },
            .mask = os.empty_sigset,
            .flags = 0,
        };
        // Ignore the SIGPIPE signal, so that we find out about
        // broken connection errors via a failure from write().
        try os.sigaction(os.SIG.PIPE, &sigact, null);
    }

    const host = try net.Address.parseIp("0.0.0.0", PORT_NUM);

    var server = net.StreamServer.init(net.StreamServer.Options{});
    defer server.deinit();
    try server.listen(host);

    while (true) {
        var client = try server.accept();
        defer client.stream.close();

        log.info("Connection from {}", .{client.address});

        var buffer: [BUF_SIZE]u8 = undefined;
        const line = try client.stream.reader().readUntilDelimiterOrEof(
            &buffer,
            '\n',
        );

        if (line) |seqNumRev| {
            const seqNumStr = try fmt.allocPrint(
                gpa.allocator(),
                "{d}\n",
                .{seqNum},
            );
            defer gpa.allocator().free(seqNumStr);

            const numWrite = try client.stream.writer().write(seqNumStr);
            if (numWrite != seqNumStr.len) {
                log.err("Error on write", .{});
            }

            // Update sequence number
            seqNum += try fmt.parseInt(usize, seqNumRev, 0);
        } else {
            client.stream.close();
            continue; // Bad request; skip it
        }
    }
}
