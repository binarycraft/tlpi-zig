//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 59-3                                                            !
//!                                                                         !
//! i6d_ucase_sv.zig                                                        !
//!                                                                         !
//! A server that receives datagrams, converts their contents to uppercase, !
//! and then returns them to the senders.                                   !
//!                                                                         !
//! See also i6d_ucase_cl.zig.                                              !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;
const net = std.net;
const log = std.log;
const ascii = std.ascii;
const print = std.debug.print;
const system = os.system;

const BUF_SIZE = 10;
const PORT_NUM = 50002;
// Wildcard address
const in6addr_any = [16]u8{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

pub fn main() !void {
    var allocator = std.heap.page_allocator;
    var claddr: net.Address = undefined;
    var buf: [BUF_SIZE]u8 = undefined;
    // Create a datagram socket bound to an address in the IPv6 domain
    const sockfd = try os.socket(
        os.AF.INET6,
        os.SOCK.DGRAM,
        0,
    );

    var svaddr = std.net.Address.initIp6(in6addr_any, PORT_NUM, 0, 0);

    try os.bind(sockfd, &svaddr.any, svaddr.getOsSockLen());

    // Receive messages, convert to uppercase, and return to client
    while (true) {
        var rev_len = @intCast(os.socklen_t, @sizeOf(os.sockaddr.in6));
        var bytesReceived = try os.recvfrom(sockfd, &buf, 0, &claddr.any, &rev_len);
        // Display address of client that sent the message
        const claddrStr = try fmt.allocPrint(allocator, "{}", .{claddr});
        print("Server received {d} bytes from ({s}, {d})\n", .{ bytesReceived, claddrStr, claddr.in6.sa.port });

        for (buf[0..bytesReceived]) |*char| {
            char.* = ascii.toUpper(char.*);
        }

        var bytesSent = try os.sendto(sockfd, buf[0..bytesReceived], 0, &claddr.any, rev_len);
        if (bytesSent != bytesReceived) {
            log.warn("Failed to send all data", .{});
        }
    }
}
