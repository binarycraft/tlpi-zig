//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                  Copyright (C) BinaryCraft, 2022.                       !
//!                                                                         !
//! This program is free software. You may use, modify, and redistribute it !
//! under the terms of the GNU General Public License as published by the   !
//! Free Software Foundation, either version 3 or (at your option) any      !
//! later version. This program is distributed without any warranty.  See   !
//! the file LICENSE for details.                                           !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!                                                                         !
//! Listing 24-1 */                                                         !
//!                                                                         !
//! t_fork.zig                                                              !
//!                                                                         !
//! Demonstrate the use of fork(), showing that parent and child            !
//! get separate copies of stack and data segments.                         !
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

const std = @import("std");
const os = std.os;

var idata: usize = 111; // Allocated in data segment

pub fn main() !void {
    var istack: usize = 222; // Allocated in stack segment

    var childPid = try os.fork();
    switch (childPid) {
        0 => {
            idata *= 3;
            istack *= 3;
        },
        else => {
            std.time.sleep(3 * 1000000000); // Give child a chance to execute
        },
    }

    // Both parent and child come here
    std.debug.print("PID={d} {s} idata={d} istack={d}\n", .{ os.system.getpid(), if (childPid == 0) "(child)" else "(parent)", idata, istack });
}
