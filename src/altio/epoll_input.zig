const std = @import("std");
const print = std.debug.print;
const os = std.os;
const mem = std.mem;

const MAX_EVENTS = 5;
const MAX_BUF = 1000;

pub fn main() !void {
    const argv = os.argv;
    var buf: [MAX_BUF]u8 = undefined;
    var evlist: [MAX_EVENTS]os.system.epoll_event = undefined;

    if (argv.len < 2 or mem.eql(u8, mem.span(argv[1]), "--help")) {
        print("{s} file...\n", .{argv[0]});
    }

    var epfd = try os.epoll_create1(0);

    for (argv[1..]) |arg| {
        const fd = try os.open(mem.span(arg), os.O.RDONLY, 0);
        print("Opened \"{s}\" on fd {d}\n", .{ arg, fd });
        var event = os.system.epoll_event{
            .events = os.system.EPOLL.IN,
            .data = os.system.epoll_data{ .fd = fd },
        };

        try os.epoll_ctl(epfd, os.system.EPOLL.CTL_ADD, fd, &event);
    }

    var numOpenFds = argv.len - 1;
    while (numOpenFds > 0) {
        print("About to epoll_wait()\n", .{});
        var ready = os.epoll_wait(epfd, &evlist, -1);
        print("Ready: {d}\n", .{ready});

        var j: usize = 0;
        while (j < ready) : (j += 1) {
            print(
                "  fd={d}; events: {s}{s}{s}\n",
                .{
                    evlist[j].data.fd,
                    if (evlist[j].events & os.system.EPOLL.IN > 0) "EPOLLIN " else "",
                    if (evlist[j].events & os.system.EPOLL.HUP > 0) "EPOLLHUP " else "",
                    if (evlist[j].events & os.system.EPOLL.ERR > 0) "EPOLLERR " else "",
                },
            );

            if (evlist[j].events & os.system.EPOLL.IN > 0) {
                var s = try os.read(evlist[j].data.fd, &buf);
                print("    read {d} bytes: {s}\n", .{ s, buf[0..s] });
            } else if (evlist[j].events & (os.system.EPOLL.HUP | os.system.EPOLL.ERR) > 0) {
                print("    closing fd {d}\n", .{evlist[j].data.fd});
                defer os.close(evlist[j].data.fd);
                numOpenFds -= 1;
            }
        }
    }

    print("All file descriptors closed; bye\n", .{});
}
